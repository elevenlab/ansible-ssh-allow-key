Ansible Role: Ssh-allow-key
===================
Add an ssh public key to the `authorized_key` on Debian server.

It will allow password-less connection to the server.

----------
Requirment
--------------

Need public ssh keys in a folder reachable from the role itself

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):
The roles look for the ssh key in the path: 

	{{ssh_akey_base_path}}/{{item}}/{{ssh_akey_namespace}}{{ssh_akey_pub_ext}}
where `item` is each element of:
	
	ssh_akey_authorized_hosts: []
A list of hosts that will be allowed to connect to the machine.
Each element of the list will be concatenated to `ssh_akey_base_path`( default `''`) to look for the key. (element doesn't have to be hostname. It only represent the directory where the ssh key is located in the local ansible provider)

	ssh_akey_namespace: 'id_rsa'
	ssh_akey_pub_ext: '.pub'
the identity name of the key and its extension

	ssh_akey_user: 'root'
the user for whom the key will be added to `authorized_key`

   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: allow passwordless access between hosts
      hosts: all
      vars:    
          ssh_akey_authorized_hosts: "{{ play_hosts }}"
          ssh_akey_user: user
          ssh_akey_base_path: 'files/{{inventory}}/{{ansible_hostname}}'
          ssh_akey_namespace: id_rsa
      roles:
          - ssh-allow-key
 
in `group_vars/all`:

	inventory: "inventory_name"
In this way you can have different ssh key for different host. 